package pcd.ass02.ex2;

/**
 * Simple Mandelbrot Set Viewer 
 *		 
 * @author aricci
 *
 */
public class MandelbrotSequentialAnimator {
	public static void main(String[] args) throws Exception {

		/* size of the mandelbrot set in pixel */
		int width = 640;
		int height = 480;

		/* number of iteration */
		int nIter = 500;

		/* region to be represented: center and radius */
		Complex c0 = new Complex(-0.75,0);
		double rad0 = 2;

		Complex c1 = new Complex(-0.75,0.1);
		Complex c2 = new Complex(-0.1011,0.9563);
		Complex c3 = new Complex(0.254,0);
		Complex c4 = new Complex(0.001643721971153, 0.822467633298876);

		/* creating the set */
		MandelbrotSetImage set = new MandelbrotSetImageImplOpt(width,height, c4, rad0);

		System.out.println("Computing w:"+width+"|h:"+height+"|nIt:"+nIter+"...");
		StopWatch cron = new StopWatch();

		/* showing the image */
		MandelbrotView view = new MandelbrotView(set,width,height);
		view.setVisible(true);

		double radius = rad0;
        double meanTime = 0.0;
        int cycles  = 0;
        double time = 0.0;
		/* simple animation */

		while (radius > Double.MIN_VALUE && cycles < 1000){
			cron.start();
			set.compute(nIter);
			cron.stop();

            ++cycles;
            time = cron.getTime();
            meanTime += time;

			System.out.println("Frame computed in "+time+" ms");
            System.out.println("Mean compute time: " + meanTime/cycles+"\n");
			radius *= 0.9;
			set.updateRadius(radius);
			view.repaint();
		}

        System.out.println("Mean compute time: " + meanTime/cycles+" over " + cycles + " cycles\n");
	}

}
