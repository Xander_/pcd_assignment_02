package pcd.ass02.ex2.executor;

import pcd.ass02.ex2.Complex;
import pcd.ass02.ex2.MandelbrotSetImage;

/**
 *
 * Created by Xander_C on 24/03/2017.
 */
public class MandelbrotSetImageExecutorImpl implements MandelbrotSetImage{

    private final Complex origin;
    private double delta;
    private final int width;
    private final int height;
    private TaskExecutorManager tm;
    private int[] imageBuffer;

    public MandelbrotSetImageExecutorImpl(final Complex origin, final double radius,
                                          final int width, final int height){

        this.origin = origin;
        this.delta = radius/(width*0.5);
        this.width = width;
        this.height = height;
        this.imageBuffer = new int[width*height];

        tm = new TaskExecutorManager(this.imageBuffer, width, height);
    }

    @Override
    public void compute(int niter) {
        tm.init( Runtime.getRuntime().availableProcessors()+1);
        tm.compute(this.origin, this.delta, niter);
        tm.closeExecution();
    }

    @Override
    public Complex getPoint(int x, int y) {
        return new Complex((x - width*0.5)*delta + origin.re(), origin.im() - (y - height*0.5)*delta);
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int[] getImage() {
        return this.imageBuffer;
    }

    @Override
    public void updateRadius(double radius) {
        this.delta = radius/(width*0.5);
    }
}
