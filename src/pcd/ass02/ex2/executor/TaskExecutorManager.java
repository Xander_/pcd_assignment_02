package pcd.ass02.ex2.executor;
import pcd.ass02.ex2.Complex;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 *
 * Created by Xander_C on 25/03/2017.
 */
public class TaskExecutorManager {

    public static int defaultChunkW = 16;
    public static int defaultChunkH = 8;


    private final int[] image;
    private final int w;
    private final int h;
    private ExecutorService executor;

    public TaskExecutorManager(final int[] image, final int w, final int h) {
        this.image = image;
        this.w = w;
        this.h = h;
    }

    public void init(final int poolSize){
        executor = new ThreadPoolExecutor(poolSize, poolSize,
                0L, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<>(this.w*this.h/(defaultChunkW*defaultChunkH)+1));
    }

    public void compute(final Complex O, final double delta, final int niter){

        final List<Future<Boolean>> ongoingTaskList = new ArrayList<>();

        for (int yFrom = 0; yFrom < h; yFrom += defaultChunkH) {
            for (int xFrom = 0; xFrom < w; xFrom += defaultChunkW) {

                final int xTo = Math.min(xFrom + defaultChunkW, w) - 1;
                final int yTo = Math.min(yFrom + defaultChunkH, h) - 1;

                ongoingTaskList.add(executor.submit(
                        new ComputeChunkTask(image, xFrom, yFrom, xTo, yTo, niter, w, h, O, delta)));
            }
        }

        for (Future<Boolean> f : ongoingTaskList){
            try {
                f.get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void closeExecution(){
        executor.shutdown();
    }
}
