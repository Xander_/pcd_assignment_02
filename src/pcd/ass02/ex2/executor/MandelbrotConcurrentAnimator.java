package pcd.ass02.ex2.executor;

import pcd.ass02.ex2.*;
import pcd.ass02.ex2.MandelbrotSetImage;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Xander_C on 24/03/2017.
 */
public class MandelbrotConcurrentAnimator {
    /* size of the mandelbrot set in pixel */
    private static final int width = 400;
    private static final int height = 400;

    public static void main(String[] args) throws Exception {

        //42.40659340659341 // 100.42950819672132

        // Mean compute time: 117.188 over 1000 cycles chunk 64*32
        // Mean compute time: 115.019 over 1000 cycles with chunk size of 16*8
        // Mean compute time: 436.595 over 1000 cycles

        final List<Complex> points = new ArrayList<>();
        final List<Double> rads = new ArrayList<>();

        /* region to be represented: center and radius */
//        points.add(new Complex(0, 0.1));
//        rads.add(2.0);

//        points.add(new Complex(-0.75, 0.1));
//        rads.add(0.02);
//
//        points.add(new Complex(0.7485, 0.0505));
//        rads.add(0.000002);
//
//        points.add(new Complex(0.254036, 0.000409));
//        rads.add(0.001);
//
//        points.add(new Complex(0.2535015, 0.0003425));
//        rads.add(0.00001);
//
        points.add(new Complex(0.001643721971153, 0.822467633298876));
        rads.add(2.0);

        for (int i = 0; i < points.size(); i++){

            System.out.println("Computing w:"+width+"|h:"+height+"|nIt:"+500+"...");
            System.out.println("\n[Point] (" + points.get(i).re()+ ", " + points.get(i).im()+")\n");
            // CONCURRENT MANDELBROT
            System.out.println("[Starting][Concurrent Computing]");

            MandelbrotSetImage wm = new MandelbrotSetImageExecutorImpl(points.get(i), rads.get(i), width, height);

            /* showing the image */
            MandelbrotConcurrentView frame = new MandelbrotConcurrentView(wm, width, height, true);
            frame.setVisible(true);
        }
    }
}
