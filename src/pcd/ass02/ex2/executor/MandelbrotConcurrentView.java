package pcd.ass02.ex2.executor;

import pcd.ass02.ex2.Complex;
import pcd.ass02.ex2.MandelbrotSetImage;
import pcd.ass02.ex2.StopWatch;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import static pcd.ass02.ex2.executor.TaskExecutorManager.*;

/**
 * Simple view of a Mandelbrot Set Image
 *
 * @author aricci
 */
public class MandelbrotConcurrentView extends JFrame implements MouseMotionListener {

    private static final int banchmarkIteration = 1000;
    public static final int niter = 500;
    private volatile double lastRadius = 2.0;

    private MandelbrotSetImage set;

    /* text fields reporting the position on the complex plain, given the mouse pointer */
    private JTextField posRe, posIm;

    private Thread repaintHandler;
    private volatile boolean isHandlerAlive = true;

    private double meanTime = 0.0;
    private int cycles  = 0;

    public MandelbrotConcurrentView(MandelbrotSetImage set, int w, int h, boolean benchmarking) {
        super("Mandelbrot Viewer");

        setSize(w, h);
        this.setResizable(false);

        this.set = set;
        MandelbrotPanel canvas = new MandelbrotPanel(set);
        canvas.setPreferredSize(new Dimension(set.getWidth(), set.getHeight()));
        JScrollPane scrollPane = new JScrollPane(canvas);

        final JPanel commands = new JPanel();
        final JButton start = new JButton("Start");
        final JButton stop = new JButton("Stop");
        final JButton close = new JButton("Exit");

        stop.setEnabled(false);

        start.addActionListener(e -> {
            isHandlerAlive = true;
            start.setEnabled(false);
            stop.setEnabled(true);

            repaintHandler = new Thread(() ->{
                StopWatch cron = new StopWatch();
                double radius = lastRadius;
                double time = 0.0;

                // Always true if not benchmarking, else it depends on the number of cycles
                boolean benchmarkCheck = !benchmarking || cycles < MandelbrotConcurrentView.banchmarkIteration;

                while (isHandlerAlive && radius >= Double.MIN_VALUE && benchmarkCheck){
                    cron.start();
                    set.compute(niter);
                    cron.stop();

                    ++cycles;
                    time = cron.getTime();
                    meanTime += time;

                    System.out.println("Magnifying Radius: " + radius);
                    System.out.println("Frame computed in " + time + " ms");

                    radius *= 0.9;
                    set.updateRadius(radius);
                    repaint();

                    benchmarkCheck = !benchmarking || cycles < MandelbrotConcurrentView.banchmarkIteration;
                }

                lastRadius = radius;
                System.out.println("\nLast Radius: " + lastRadius);

                if (benchmarking){
                    System.out.println("Mean concurrent compute time: " + meanTime/cycles + "ms over " + cycles + " cycles " +
                            "with chunk size of " + defaultChunkW + "*" + defaultChunkH +"\n");
                }

            });

            repaintHandler.start();
        });

        stop.addActionListener(e ->{
            isHandlerAlive = false;
            stop.setEnabled(false);
            start.setEnabled(true);
        });

        close.addActionListener(e -> {
            isHandlerAlive = false;
            System.exit(1);
        });

        commands.add(start);
        commands.add(stop);
        commands.add(close);

        final JPanel info = new JPanel();
        posRe = new JTextField(15);
        posIm = new JTextField(15);
        posRe.setEditable(false);
        posIm.setEditable(false);
        info.add(new JLabel("Re: "));
        info.add(posRe);
        info.add(new JLabel("Im: "));
        info.add(posIm);

        setLayout(new BorderLayout());
        add(scrollPane, BorderLayout.CENTER);
        add(info, BorderLayout.NORTH);
        add(commands, BorderLayout.SOUTH);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        canvas.addMouseMotionListener(this);
    }

    @Override
    public void mouseDragged(MouseEvent e) {}

    /**
     * When the mouse is moved, the position on the complex plane is updated
     */
    public void mouseMoved(MouseEvent e) {
        Complex point = set.getPoint(e.getX(), e.getY());
        posRe.setText(String.format("%.10f", point.re()));
        posIm.setText(String.format("%.10f", point.im()));
    }

    private class MandelbrotPanel extends JPanel {

        private MandelbrotSetImage set;
        private BufferedImage image;

        public MandelbrotPanel(MandelbrotSetImage set) {
            this.set = set;
            image = new BufferedImage(set.getWidth(), set.getHeight(), BufferedImage.TYPE_INT_RGB);
        }

        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            image.setRGB(0, 0, set.getWidth(), set.getHeight(), set.getImage(), 0, set.getWidth());
            g2.drawImage(image, 0, 0, null);
        }
    }
}