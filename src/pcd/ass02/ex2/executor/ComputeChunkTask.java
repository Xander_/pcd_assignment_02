package pcd.ass02.ex2.executor;

import pcd.ass02.ex2.Complex;

import java.util.concurrent.Callable;

/**
 *
 * Created by Xander_C on 22/03/2017.
 */
public class ComputeChunkTask implements Callable<Boolean> {

    private final int[] imageBuffer;
    private final int xFrom;
    private final int yFrom;
    private final int xTo;
    private final int yTo;
    private final int maxIter;
    private final int width;
    private final int height;
    private final Complex origin;
    private final double delta;

    public ComputeChunkTask(final int[] imageBuffer,
                            final int xFrom, final int yFrom, final int xTo, final int yTo,
                            final int maxIter, final int w, final int h,
                            final Complex origin, final double delta) {

        this.imageBuffer = imageBuffer;
        this.xFrom = xFrom;
        this.yFrom = yFrom;
        this.xTo = xTo;
        this.yTo = yTo;
        this.maxIter = maxIter;
        this.width = w;
        this.height = h;
        this.origin = origin;
        this.delta = delta;
    }

    @Override
    public Boolean call() {

        for (int y = yFrom; y <= yTo; y++) {
            for (int x = xFrom; x <= xTo; x++) {

                double x0 = (x - width * 0.5) * delta + origin.re();
                double y0 = origin.im() - (y - height * 0.5) * delta;
                double level = computeColor(x0, y0, maxIter);
                int color = (int) (level * 255);
                int effectiveColor = color + (color << 8) + (color << 16);

                imageBuffer[y * width + x] = effectiveColor;
            }
        }
        return true;
    }

    private double computeColor(final double x0, final double y0, final int maxIteration) {
        int iters = 0;
        double x1 = 0;
        double y1 = 0;
        double x2 = x1 * x1;
        double y2 = y1 * y1;

        while (x2 + y2 < 4 && iters < maxIteration) {
            double xtemp = x2 - y2 + x0;
            y1 = 2 * x1 * y1 + y0;
            x1 = xtemp;
            x2 = x1 * x1;
            y2 = y1 * y1;
            iters++;
        }

        if (iters == maxIteration) {
              /* the point belongs to the set */
            return 0;
        } else {
              /* the point does not belong to the set => distance */
            return 1.0 - ((double) iters) / maxIteration;
        }
    }
}
