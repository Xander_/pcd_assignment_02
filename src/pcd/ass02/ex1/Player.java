package pcd.ass02.ex1;

import javafx.util.Pair;

import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

import static java.text.MessageFormat.format;

/**
 *
 * Created by Xander_C on 13/04/2017.
 */
public class Player implements Runnable {

    private int ID;
    private final OracleInterface oracle;

    private Pair<Long, Long> offset;
    private long lastGuess;
    private Optional<Result> lastRes;

    public Player(final int ID, final OracleInterface oracle, final long seed) {
        this.ID = ID;
        this.oracle = oracle;
        this.offset = new Pair<>(0L, seed);
        this.lastRes = Optional.empty();
    }

    @Override
    public void run() {

        String endGameMessage = "Sob.";

        while (!oracle.isGameFinished()) {
            Optional<Result> tempRes = Optional.empty();
            try {

                log("[Player_{0}] Guessing...", ID);

                final long guess = this.guessManagementSystem();
                tempRes = Optional.of(oracle.tryToGuess(ID, guess));
                this.lastGuess = guess;

            } catch (GameFinishedException e) {
                if (e.getWinnerID() == this.ID) {
                    endGameMessage = "Won!";
                }

            } finally {
                /*
                If the exception is thrown then the res value will be empty,
                'cause the try statement will fail thus res will not be re-initialized
                In this case I'll only let the first one of the other player inside the queue
                to interact with the Oracle
                */
                if (tempRes.isPresent()) {
                    lastRes = tempRes;
                }
            }
        }

        log("[Player_{0}] {1}", ID, endGameMessage);
    }

    private long guessManagementSystem() {

        Optional<Long> actualGuess = Optional.empty();

        if (lastRes.isPresent()) {

            if (lastRes.get().isGreater()) {
                this.offset = new Pair<>(lastGuess, offset.getValue());
            } else {
                this.offset = new Pair<>(offset.getKey(), lastGuess);
            }

            actualGuess = Optional.of(offset.getKey()/2 + offset.getValue()/2);

        } else {
            // Genero il primo guess in maniera randomica finchè non ne trovo uno soddisfacente
            while (!actualGuess.isPresent() || actualGuess.get() < 0){
                actualGuess = Optional.of(ThreadLocalRandom.current().nextLong(offset.getKey(), offset.getValue()));
            }
        }

        return actualGuess.get();
    }

    private static void log(final String pattern, Object... args){
        System.out.println(format(pattern, args));
    }
}
