package pcd.ass02.ex1;

public class GameFinishedException extends Exception {

    private final int playerID;

    public GameFinishedException(final String msg, final int playerID){
        super(msg);
        this.playerID = playerID;
    }


    public int getWinnerID() {
        return playerID;
    }
}
