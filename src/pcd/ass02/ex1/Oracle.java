package pcd.ass02.ex1;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.ReentrantLock;

import static java.text.MessageFormat.format;

/**
 *
 * Created by Xander_C on 13/04/2017.
 */
public class Oracle implements OracleInterface {

    private final static String name = "Cassandra";

    private final long numberToBeGuessed;
    private final int nPlayers;
    private int turn = 0;

    private GameFinishedException gameFinishedException;

    private final ReentrantLock waitingRoom;
    private boolean someoneHasWon = false;
    private boolean iCanGo = true;

    private final Set<Integer> visitors;

    public Oracle(final long seed, final int nPlayers){

        this.numberToBeGuessed = ThreadLocalRandom.current().nextLong(0L, Math.abs(seed));
        this.nPlayers = nPlayers;

        this.waitingRoom = new ReentrantLock();
        this.visitors = new HashSet<>(nPlayers);

        log("Selected Magic Number is : {0}", numberToBeGuessed);
    }

    @Override
    public synchronized boolean isGameFinished() {
        return this.someoneHasWon;
    }

    @Override
    public synchronized Result tryToGuess(int playerID, long value) throws GameFinishedException {

        // Se entro nel Monitor ho già completato il seguente turno => Devo attendere al fine del turno attuale
        // I can go è necessaria nel momento in cui ho molti player che sono in attesa dal turno precedente =>
        // Non posso farli uscire tutti
        while (!this.iCanGo() || visitors.contains(playerID)){
            log("[Player_{0}] Waiting for {1}...", playerID, name);

            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (!this.isGameFinished() && !visitors.contains(playerID)){
                    // Quando sblocco i threads devo far in modo che solo 1 esca realmente
                    waitingRoom.lock();

                    if (this.iCanGo()) {
                        break;
                    }

                } else if (this.isGameFinished()) {
                    break;
                }
            }
        }

        final Result res = new Result(value, numberToBeGuessed);

        // A questo punto i thread non dovrebbero entrare in modo concorrente in questa sezione
        if (!this.isGameFinished()){
            // blocco in modo sale gli altri contendenti
            this.iCanGo = false;
            // lascio che un'altro thread entri nella sala d'attesa
            if (waitingRoom.isHeldByCurrentThread()) {
                waitingRoom.unlock();
            }

            // Aggiungo il mio ID al turno corrente
            this.visitors.add(playerID);

            log("[Player_{0}] I guess the magic number is {1}", playerID, value);

            if (res.found()) {
                this.someoneHasWon = true;
                this.gameFinishedException = new GameFinishedException("", playerID);
                log("[{0}] We have a winner: [Player_{1}] : {2}", name, playerID, value);
            } else {
                log("[{0}] Wrong guess from [Player_{1}] : {2}", name, playerID, value);
            }
        }

        // Sono un thread e sono l'ultimo di questo turno,
        // rimuovo dal set tutti gli altri player che hanno già finito questo turno
        if (visitors.size() == this.nPlayers || this.isGameFinished()){
            visitors.removeIf(i -> true);
            if (!this.isGameFinished()) log("[{0}] End of turn: {1}\n", name, turn++);
        }

        this.iCanGo = true;
        this.notifyAll();

        if (!this.isGameFinished()) return res; else throw gameFinishedException;
    }

    private synchronized boolean iCanGo(){
        return this.iCanGo;
    }

    private static void log(final String pattern, Object... args){
        System.out.println(format(pattern, args));
    }
}
