package pcd.ass02.ex1;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Xander_C on 13/04/2017.
 */
public class TestGuessTheNumber {

    public static final int N_PLAYERS = 10;

    public static void main(String[] argv){

        final OracleInterface oracle = new Oracle(Long.MAX_VALUE/100000, N_PLAYERS);

        final List<Thread> players = new ArrayList<>();

        for(int i = 0; i < N_PLAYERS; i++){
            players.add(new Thread(new Player(i, oracle, Long.MAX_VALUE/100000)));
        }

        for (Thread p : players) {
            p.start();
        }

        for (Thread p : players){
            try {
                p.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
