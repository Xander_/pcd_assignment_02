package pcd.ass02.ex1;

public class Result {

    private long guess;
    private long realValue;

    public Result(final long guess, final long realValue){

        this.guess = guess;
        this.realValue = realValue;
    }

    boolean found(){
        return guess == realValue;
    }

    boolean isGreater(){
        return realValue > guess;
    }

    boolean isLess(){
        return realValue < guess;
    }
}
